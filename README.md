# Multimedia Chat Server

## Run servers
# turn-server(port: 3478-3479/tcp/udp 5349-5350/tcp/udp 40000-45000/udp)
    $ cd turn-server
    $ ./create_user.sh
    $ sudo ./mobile_relay.sh

# room-server(port: 8080)
    $ cd room-server
    $ ./run-apprtc.sh

# signaling-server(port: 8090)
    $ cd room-server
    $ ./run-collider.sh

# firewall setting for running (TODO)
    

## Build servers

### turn-server
    - See turn-server/README.turn.md

### room-server
    $ cd room-server/apprtc-git
    $ npm install
    $ npm run build

### signaling-server (same as Run)
    $ cd room-server
    $ ./build-collider.sh


http://openshift.github.io/documentation/oo_cartridge_guide.html#tomcat

### MySQL 
    - connection url: Connection URL: mysql://$OPENSHIFT_MYSQL_DB_HOST:$OPENSHIFT_MYSQL_DB_PORT/
    - Database Name: mmchat
