#!/bin/bash

# See http://www.rackspace.com/knowledge_center/article/generate-a-csr-with-openssl
# See https://www.tbs-certificates.co.uk/FAQ/en/192.html
# CSR Field
# /C=	Country	GB
# /ST=	State	London
# /L=	Location	London
# /O=	Organization	Global Security
# /OU=	Organizational Unit	IT Department
# /CN=	Common Name	example.com

# openssl genrsa -out ./pkey.pem 2048
KEYFILE=./pkey.pem
CSRFILE=./cert.csr
SUBJECT="/C=KR/ST=Seoul/L=Seoul/O=Usoftation/OU=RnD/CN=usoftation.com"

openssl req -nodes -newkey rsa:2048 -keyout $KEYFILE -out $CSRFILE -subj $SUBJECT

