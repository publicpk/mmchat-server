# Chat Server
======================

## Directory 
----------------------------
```
    /chat-server
       /node_modules
       :
    /easyrtc.git
```
 
Installing Required Modules:
----------------------------
```
    $> npm install
    $> npm run init
```

Running the Server:
-------------------
```
    $> npm start
```

Viewing the examples:
---------------------
    - http://localhost:8090/

