#!/bin/bash

DEST_DIR=../easyrtc.git
DIFF_FILE=easyrtc-working.diff
WORK_DIR=$PWD

function gen_diff() {
    pushd $DEST_DIR
    git diff --no-prefix > $WORK_DIR/$DIFF_FILE
    popd
}

function apply_diff() {
    pushd $DEST_DIR
    patch -p0 < $WORK_DIR/$DIFF_FILE
    popd
}

if [ "$1" == "gen" ]; then
    echo "Generate diff file '$WORK_DIR/$DIFF_FILE' to '$DEST_DIR'"
    gen_diff
elif [ "$1" == "apply" ]; then
    echo "Apply diff file '$WORK_DIR/$DIFF_FILE' to '$DEST_DIR'"
    apply_diff
else
    echo "Usage: $0 gen|apply"
    exit 0
fi
