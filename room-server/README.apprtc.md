#
# apprtc server (room server, signaling server)
#

## download sources from git repository
    $ git clone https://github.com/webrtc/apprtc.git apprtc-git

## install tools
    $ cd apprtc-tools
    $ ./install.sh

## Build Room Server on Ubuntu
    
### install dependencies
    - nodejs
    $ sudo apt-get install nodejs

    - python 2.7
    $ sudo apt-get install python

### build & install
    $ cd apprtc-git
    $ npm install
    $ npm run build

### run (default port: 8080)
    $ ./run-apprtc.sh
    # or 
    $ GAEROOT="/new/path/to/gaeroot" ./run-apprtc.sh


## Build Signaling Server on Ubuntu

### build & run (default port: 8090)
    $ ./run-collider.sh
    # or
    $ GOROOT="/new/path/to/go" ./run-collider.sh


## Interface of collider(Signaling server)
### Websocket interface
  - 'WebSocket' /ws ${JSON_CONTENT}
  - {Cmd:'register',RoomID:${RoomID},ClientID:${ClientID}}:
    register the client to the room
  - {Cmd:'send',RoomID:${RoomID},ClientID:${ClientID},Msg:${Msg}}:
    relay the message to the room

### HTTP interface
  - 'POST' /*/${RoomID}/${ClientID}: relay the content of body to a room
  - 'DELETE' /*/${RoomID}/${ClientID}: unregister the client to the room
  - 'GET' /status: return the current status


