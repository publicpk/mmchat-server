#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#
# install Google App Engine SDK for Python
# [Google App Engine SDK for Python](https://cloud.google.com/appengine/downloads#Google_App_Engine_SDK_for_Python)
#
BIN=google_appengine_1.9.30.zip
NAME=google_appengine

[ ! -e "$NAME" ] && [ ! -e "$BIN" ] && curl -L -O https://storage.googleapis.com/appengine-sdks/featured/$BIN
[ ! -e "$NAME" ] && unzip $BIN
GAEROOT=$DIR/$NAME
echo "GAEROOT=$GAEROOT"

#
# install go
# [Go Programming Language](https://golang.org/doc/install)
#
BIN=go1.5.2.linux-amd64.tar.gz
NAME=go

[ ! -e "$NAME" ] && [ ! -e "$BIN" ] && curl -L -O https://storage.googleapis.com/golang/$BIN
[ ! -e "$NAME" ] && tar -xzf $BIN
GOROOT=$DIR/$NAME
echo "GOROOT=$GOROOT"

