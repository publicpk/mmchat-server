#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

[ -z "$GOROOT" ] && export GOROOT=$DIR/apprtc-tools/go
export PATH=$GOROOT/bin:$PATH
export GOPATH=$DIR/apprtc-git/out

[ ! -e $GOPATH/src ] && mkdir $GOPATH/src
[ ! -e $GOPATH/src/collider ] && ln -s $DIR/apprtc-git/src/collider/collider $GOPATH/src/
[ ! -e $GOPATH/src/collidermain ] && ln -s $DIR/apprtc-git/src/collider/collidermain $GOPATH/src/
[ ! -e $GOPATH/src/collidertest ] && ln -s $DIR/apprtc-git/src/collider/collidertest $GOPATH/src/

# Install dependencies
[ ! -d $GOPATH/pkg ] && go get collidermain
# Install `collidermain`
#[ ! -d $GOPATH/bin ] && go install collidermain
go install collidermain


