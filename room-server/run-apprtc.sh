#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

[ -z "$GAEROOT" ] && export GAEROOT=$DIR/apprtc-tools/google_appengine
export PATH=$GAEROOT:$PATH
export APPRTCROOT=$DIR/apprtc-git/out/app_engine

$GAEROOT/dev_appserver.py --host 0.0.0.0 $APPRTCROOT

