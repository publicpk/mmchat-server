#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

[ -z "$GOROOT" ] && export GOROOT=$DIR/apprtc-tools/go
export PATH=$GOROOT/bin:$PATH
export GOPATH=$DIR/apprtc-git/out

# Running
# $GOPATH/bin/collidermain -port=8090 -tls=true
$GOPATH/bin/collidermain -port=8090 -tls=false

## Testing
#go test collider


