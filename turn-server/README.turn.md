# coturn server

## Build on Ubuntu

### git repository
    $ git clone https://github.com/coturn/rfc5766-turn-server.git rfc5766-turn-server-git
    # $ git clone https://github.com/coturn/coturn.git coturn-git

### install dependencies
    $ sudo apt-get install libevent-dev libpq-dev

### build & install
    $ cd rfc5766-turn-server-git
    $ ./configure --prefix=$HOME/.local
    $ make && make install

### run
    $ ./mobile_relay.sh
    or
    $ emacs $HOME/.local/etc/turnserver.conf # edit config file
    $ turnserver

## Turn Server manuals
    - See http://manpages.ubuntu.com/manpages/saucy/man1/rfc5766-turn-server.1.html
    
### turnserver
    - TURN Server relay.

### turnadmin
    - TURN administration tool.

