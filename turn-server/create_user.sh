#!/bin/sh
# see https://github.com/coturn/rfc5766-turn-server/blob/v3.2/README.turnserver

[ -z "$TURN_HOME" ] && export TURN_HOME="$HOME/.local"

LD_LIBRARY_PATH="$TURN_HOME/lib:$LD_LIBRARY_PATH" PATH="$TURN_HOME/bin:${PATH}" turnadmin -k -u ustmmchatuser1 -r north.gov -p password_for_ustmmchatuser1
