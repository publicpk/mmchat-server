#!/bin/sh
# see https://github.com/coturn/rfc5766-turn-server/blob/v3.2/README.turnserver

[ -z "$TURN_HOME" ] && export TURN_HOME="$HOME/.local"

LD_LIBRARY_PATH="$TURN_HOME/lib:$LD_LIBRARY_PATH" PATH="$TURN_HOME/bin:${PATH}" turnserver -v -c mobile_relay.conf

# --syslog --lt-cred-mech -L 127.0.0.1 -L ::1 -E 127.0.0.1 -E ::1 --max-bps=3000000 -f -m 10 --min-port=55000 --max-port=60000 --user=ustmmchatuser1:0x7af2bcf46008cf5087452e29e6632f83 -r north.gov --cert=turn_server_cert.pem --pkey=turn_server_pkey.pem --log-file=stdout -v --mobility --cipher-list=ALL $@

#
# user creation
#
# turnadmin -k -u ustmmchatuser1 -r north.gov -p password_for_ustmmchatuser1

#
# cert creation
# 
# openssl req -x509 -newkey rsa:2048 -keyout pkey.pem -out cert.pem -days XXX -nodes
